function initMap() {
    if (!document.getElementById('js-apart-single-map')) return false;
    var mapLat = $('#js-apart-single-map').data('map-lat');
    var mapLng = $('#js-apart-single-map').data('map-lng');

    var uluru = {lat: mapLat, lng:  mapLng};

    var map = new google.maps.Map(document.getElementById('js-apart-single-map'), {
        zoom: 14,
        center: uluru
    });
    var marker = new google.maps.Marker({
        position: uluru,
        map: map,
        animation: google.maps.Animation.DROP,
        icon: './img/apart-single-map-marker.png'
    });
}


