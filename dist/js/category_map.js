
// var obj = {
//     first: [50.440511, 30.520108 , '/slid-s-2.jpg' , 'test', '$ 25 ', 1, 'Срочно Квартира Новопече... ',  'Печерский', '2', '15', '$ 3.000', './'],
//     second: [ 50.439479, 30.515913 , '/slid-s-1.jpg' , 'test', '$ 23 ', 2, 'Срочно Квартира Новопече... ', 'Печерский', '2', '15', '$ 3.000', './' ]
// }
//
var obj = {
    mainMarker: {
        latitude: 50.439479,
        longitude:30.515913,
        zoom: 14
    },
    mapPoint: {
        marker_1: {
            latitude:  50.440511,
            longitude:  30.520108,
            img:  'slid-s-2.jpg',
            label: 'test',
            price: '$ 23.00',
            id: 1,
            title:  'Срочно Квартира Новопече... ',
            region:   'Печерский1',
            room: 2,
            link:  './',
            priceM: '$ 3.000',
            dimensions: '57 м'
        },
        marker_2: {
            latitude:  50.439479,
            longitude:  30.515913,
            img:  'slid-s-1.jpg',
            label: 'test',
            price: '$ 25.00',
            id: 2,
            title:  'Срочно Квартира Новопече... ',
            region:   'Печерский',
            room: 2,
            link:  './',
            priceM: '$ 3.000',
            dimensions: '55 м'
        }
    }
};

if ($('#js-map-container').length > 0){

    google.maps.event.addDomListener(window, 'load', initMap)
}

var ib = new InfoBox();

function initMap() {
    var map = new google.maps.Map(document.getElementById('js-map-container'), {
        center: new google.maps.LatLng(obj.mainMarker.latitude, obj.mainMarker.longitude),
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        scrollwheel: false,
        zoom: obj.mainMarker.zoom,

    });

    google.maps.event.addListener(map, "click", function() { ib.close() });

    setMarkers(map, obj.mapPoint);

}


function setMarkers (map, markers) {
    for (key in markers){
        createMarker(markers[key], map);
    }
}



function createMarker(site, map){

    var marker = new MarkerWithLabel({
        position: new google.maps.LatLng(site.latitude, site.longitude),
        map: map,
        draggable: false,
        raiseOnDrag: true,
        labelContent: site.price,
        labelAnchor: new google.maps.Point(40, 40),
        labelClass: "map-labels map-labels-"+ site.id +"", // the CSS class for the label
        labelInBackground: false,
        icon: ' '
    });

    // Begin example code to get custom infobox
    var boxText = document.createElement("div");
    boxText.style.cssText = " margin-top: 0px; padding: 0px;";
    boxText.innerHTML = '<a class="box-info-map-link" href="'+ site.link +'"><div class="box-info-map">'+
                                '<div class="box-info-img bg-option" style=" background-image:url(\'./img/' + site.img + '\') "></div>'+
                                '<div class="box-info-map-box-content">'+
                                    '<div class="box-info-t">'+site.title+'</div>'+
                                    '<div class="box-info-params __region">Paйон:  <span class="__bold">'+ site.region +'</span></div>'+
                                    '<div class="box-info-tr">'+
                                        '<div class="box-info-params box-info-td">Кол-во комнат:  '+ site.room +'</div>'+
                                        '<div class="box-info-params __bold box-info-td __right">'+ site.price +'</div>'+
                                    '</div>'+
                                    '<div class="box-info-tr">'+
                                        '<div class="box-info-params box-info-td">Габариты:  '+ site.dimensions +'</div>'+
                                        '<div class="box-info-params __bold box-info-td __right">'+ site.priceM+' кв/м</div>'+
                                    '</div>'+
                                '</div>'+
                        '</div></a>';

    var myOptions = {
        content: boxText,
        disableAutoPan: false,
        maxWidth: 0,
        pixelOffset: new google.maps.Size(-130, -360),
        zIndex: null,
        boxStyle: {
            background: "url('http://google-maps-utility-library-v3.googlecode.com/svn/tags/infobox/1.1.12/examples/tipbox.gif') no-repeat",
            opacity: 1,
            width: "260px"
        },
        closeBoxMargin: "0xp",
        closeBoxURL: "http://www.google.com/intl/en_us/mapfiles/close.gif",
        infoBoxClearance: new google.maps.Size(1, 1),
        isHidden: false,
        pane: "floatPane",
        enableEventPropagation: false
    };
    // end example code for custom infobox

    google.maps.event.addListener(marker, "click", function (e) {
        ib.close();
        ib.setOptions(myOptions);
        ib.open(map, this);
    });
    return marker;
}



function pinSymbol(color) {
    return {
        path: 'M 0,0 C -2,-20 -10,-22 -10,-30 A 10,10 0 1,1 10,-30 C 10,-22 2,-20 0,0 z',
        fillColor: color,
        fillOpacity: 1,
        strokeColor: '#000',
        strokeWeight: 2,
        scale: 2
    };
}

$(document).ready(function(){
    $(".js-hover-map").hover(function(){
        var id = $(this).data('id');
        $('.map .map-labels-'+ id +'').addClass('map-hover');
    }, function(){
        var id = $(this).data('id');
        $('.map .map-labels-'+ id +'').removeClass('map-hover');
    });
});

