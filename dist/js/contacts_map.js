jQuery(document).ready(function ($) {

    //Местоположение: долгота, широта и коэффициент увеличения
    var markerMap = {
        mainMarker: {
            latitude: 50.45677,
            longitude: 30.491969,
            zoom: 16
        },
        mapPoint: {
            marker_1: {
                latitude:  50.45677,
                longitude:  30.491969
            },
            marker_2: {
                latitude:  50.455126,
                longitude:  30.493944
            },
            // marker_3: {
            //     latitude:  50.434453,
            //     longitude:  30.480744
            // },
            // marker_4: {
            //     latitude:  50.4016486,
            //     longitude:  30.6144997
            // }
        }
    };

    //Адрес до иконки с маркером
    var marker_url_active ='./img/contacts-map-marker.png';
    var marker_url =  './img/apart-single-map-marker.png';

    //Стили для элементов на карте
    var style = [
        {
            featureType: "all",
            "stylers": [
                { "saturation": -50},
                { "lightness": -30 },
            ]
        }
    ];

    //Создание точки на карте
    var map_options = {
        center: new google.maps.LatLng(markerMap.mainMarker.latitude, markerMap.mainMarker.longitude),
        zoom: markerMap.mainMarker.zoom,
        panControl: false,
        zoomControl: true,
        mapTypeControl: false,
        streetViewControl: false,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        scrollwheel: false,
        styles: style
    }

    // var map_options2 = {
    //     center: new google.maps.LatLng(markerMap.mainMarker.latitude, markerMap.mainMarker.longitude),
    //     zoom: markerMap.mainMarker.zoom,
    //     panControl: false,
    //     zoomControl: false,
    //     mapTypeControl: false,
    //     streetViewControl: false,
    //     mapTypeId: google.maps.MapTypeId.ROADMAP,
    //     scrollwheel: false,
    //     styles: style
    // }


    //Инициализация карты
    var map = new google.maps.Map(document.getElementById('contacts-map'), map_options);


    //Добавляем свой маркер местонахождения на карту (свою иконку)
    var  HELP,
        ICON,
        MarkArray = {};
    for (key in markerMap.mapPoint) {

        if(!HELP){
            ICON = marker_url_active;
            HELP = true;
        } else {
            ICON = marker_url;
        }
        MarkArray[key] = new google.maps.Marker({
            position: new google.maps.LatLng(markerMap.mapPoint[key].latitude, markerMap.mapPoint[key].longitude),
            map: map,
            visible: true,
            icon: ICON,
        });

    }

    var zoomControlDiv = document.createElement('div');

    //Помещаем кнопки увеличить/уменьшить на карту в левый верхний угол
    map.controls[google.maps.ControlPosition.LEFT_TOP].push(zoomControlDiv);


    $('body').on('click','.js-contacts-marker-next',function () {

        var index = $('.contacts-marker-box-single.active').index();
        var indexPrev = $('.contacts-marker-box-single.half-active').index();
        $('.contacts-marker-box-single.half-active').removeClass('half-active').hide();
        $('.contacts-marker-box-single.active').removeClass('active').hide();

        if(index == $('.contacts-marker-box-single').length -1 ) {
            $('.contacts-marker-box-single:eq(0)').addClass('half-active').stop(true, true).fadeIn('8000');
            $('.contacts-marker-box-single:eq(1)').addClass('active').stop(true, true).fadeIn('8000');
        }else{
            index++;
            indexPrev++;
            $('.contacts-marker-box-single:eq('+ index+' )').addClass('active').stop(true, true).fadeIn('8000');
            $('.contacts-marker-box-single:eq('+ indexPrev+' )').addClass('half-active').stop(true, true).fadeIn('8000');

        }
        moveMapTo($('.contacts-marker-box-single.active').data('marker'));
    });

    function moveMapTo(marker) {
        map.panTo(new google.maps.LatLng(markerMap.mapPoint[marker].latitude, markerMap.mapPoint[marker].longitude));

        var mkr = eval(MarkArray[marker]);

        for(key in markerMap.mapPoint){
            var mk = eval(MarkArray[key]);
            mk.setIcon({url: marker_url});
        }
         mkr.setIcon({url: marker_url_active});
    };
});




