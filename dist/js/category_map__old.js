
var obj = {
    first: [50.440511, 30.520108 , '/slid-s-2.jpg' , 'test', '$ 25 ', 5, 'Срочно Квартира Новопече... ',  'Печерский', '2', '15', '$ 3.000', './'],
    second: [ 50.439479, 30.515913 , '/slid-s-1.jpg' , 'test', '$ 23 ', 4, 'Срочно Квартира Новопече... ', 'Печерский', '2', '15', '$ 3.000', './' ]
}
if ($('#js-map-container').length > 0){
    google.maps.event.addDomListener(window, 'load', initMap)
}
var elemHeight = 280;
var elemWidth = 266;
if($(window).width() < 768){
    elemHeight = 200;
    elemWidth  = 220;
}


function InfoBox(opts) {
    google.maps.OverlayView.call(this);
    this.latlng_ = opts.latlng;
    this.map_ = opts.map;
    this.content = opts.content;
    this.offsetVertical_ = $('infobox').height() -75;
    this.offsetHorizontal_ = 55;
    this.height_ = elemHeight;
    this.width_ = elemWidth;
    var me = this;
    this.boundsChangedListener_ =
        google.maps.event.addListener(this.map_, "bounds_changed", function () {
            return me.panMap.apply(me);
        });
    // Once the properties of this OverlayView are initialized, set its map so
    // that we can display it. This will trigger calls to panes_changed and
    // draw.
    this.setMap(this.map_);
}
/* InfoBox extends GOverlay class from the Google Maps API
 */
InfoBox.prototype = new google.maps.OverlayView();
/* Creates the DIV representing this InfoBox
 */
InfoBox.prototype.remove = function () {
    if (this.div_) {
        this.div_.parentNode.removeChild(this.div_);
        this.div_ = null;
    }
};
/* Redraw the Bar based on the current projection and zoom level
 */
InfoBox.prototype.draw = function () {
    // Creates the element if it doesn't exist already.
    this.createElement();
    if (!this.div_) return;
    // Calculate the DIV coordinates of two opposite corners of our bounds to
    // get the size and position of our Bar
    var pixPosition = this.getProjection().fromLatLngToDivPixel(this.latlng_);
    if (!pixPosition) return;
    // Now position our DIV based on the DIV coordinates of our bounds
    this.div_.style.width = this.width_ + "px";
    this.div_.style.left = (pixPosition.x + this.offsetHorizontal_) + "px";
    this.div_.style.height = this.height_ + "px";
    this.div_.style.top = (pixPosition.y + this.offsetVertical_) + "px";
    this.div_.style.display = 'block';
};
/* Creates the DIV representing this InfoBox in the floatPane. If the panes
 * object, retrieved by calling getPanes, is null, remove the element from the
 * DOM. If the div exists, but its parent is not the floatPane, move the div
 * to the new pane.
 * Called from within draw. Alternatively, this can be called specifically on
 * a panes_changed event.
 */
InfoBox.prototype.createElement = function () {
    test = this;
    var panes = this.getPanes();
    var div = this.div_;
    if (!div) {
        // This does not handle changing panes. You can set the map to be null and
        // then reset the map to move the div.
        div = this.div_ = document.createElement("div");
        div.className = "infobox"
        var contentDiv = document.createElement("div");
        contentDiv.className = "content"
        contentDiv.innerHTML = this.content;
        var closeBox = document.createElement("div");
        closeBox.className = "close-map";
        closeBox.innerHTML = "<img src='img/point_click.png'>";
        div.appendChild(closeBox);

        function removeInfoBox(ib) {
            return function () {
                ib.setMap(null);
            };
        }

        google.maps.event.addDomListener(closeBox, 'click', removeInfoBox(this));
        div.appendChild(contentDiv);
        div.style.display = 'none';
        panes.floatPane.appendChild(div);
        this.panMap();
    } else if (div.parentNode != panes.floatPane) {
        // The panes have changed. Move the div.
        div.parentNode.removeChild(div);
        panes.floatPane.appendChild(div);
    } else {
        // The panes have not changed, so no need to create or move the div.
    }
}
/* Pan the map to fit the InfoBox.
 */
InfoBox.prototype.panMap = function () {
    // if we go beyond map, pan map
    var map = this.map_;
    var bounds = map.getBounds();
    if (!bounds) return;
    // The position of the infowindow
    var position = this.latlng_;
    // The dimension of the infowindow
    var iwWidth = this.width_;
    var iwHeight = this.height_;
    // The offset position of the infowindow
    var iwOffsetX = this.offsetHorizontal_;
    var iwOffsetY = this.offsetVertical_;
    // Padding on the infowindow
    var padX = 40;
    var padY = 40;
    // The degrees per pixel
    var mapDiv = map.getDiv();
    var mapWidth = mapDiv.offsetWidth;
    var mapHeight = mapDiv.offsetHeight;
    var boundsSpan = bounds.toSpan();
    var longSpan = boundsSpan.lng();
    var latSpan = boundsSpan.lat();
    var degPixelX = longSpan / mapWidth;
    var degPixelY = latSpan / mapHeight;
    // The bounds of the map
    var mapWestLng = bounds.getSouthWest().lng();
    var mapEastLng = bounds.getNorthEast().lng();
    var mapNorthLat = bounds.getNorthEast().lat();
    var mapSouthLat = bounds.getSouthWest().lat();
    // The bounds of the infowindow
    var iwWestLng = position.lng() + (iwOffsetX - padX) * degPixelX;
    var iwEastLng = position.lng() + (iwOffsetX + iwWidth + padX) * degPixelX;
    var iwNorthLat = position.lat() - (iwOffsetY - padY) * degPixelY;
    var iwSouthLat = position.lat() - (iwOffsetY + iwHeight + padY) * degPixelY;
    // calculate center shift
    var shiftLng =
        (iwWestLng < mapWestLng ? mapWestLng - iwWestLng : 0) +
        (iwEastLng > mapEastLng ? mapEastLng - iwEastLng : 0);
    var shiftLat =
        (iwNorthLat > mapNorthLat ? mapNorthLat - iwNorthLat : 0) +
        (iwSouthLat < mapSouthLat ? mapSouthLat - iwSouthLat : 0);
    // The center of the map
    var center = map.getCenter();
    // The new map center
    var centerX = center.lng() - shiftLng;
    var centerY = center.lat() - shiftLat;
    // center the map to the new shifted center
    map.setCenter(new google.maps.LatLng(centerY, centerX));
    // Remove the listener after panning is complete.
    google.maps.event.removeListener(this.boundsChangedListener_);
    this.boundsChangedListener_ = null;
};
//var MAP_MARKERS = {}

function initMap() {
    var latLng = new google.maps.LatLng(50.439479, 30.515913);


    //var homeLatLng = new google.maps.LatLng(49.47805, -123.84716);


    var map = new google.maps.Map(document.getElementById('js-map-container'), {
        zoom: 14,
        center: latLng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    for (key in obj) {
        var marker = new MarkerWithLabel({
            position: new google.maps.LatLng(obj[key][0], obj[key][1]),
            map: map,
            draggable: true,
            raiseOnDrag: true,
            labelContent: obj[key][4],
            labelAnchor: new google.maps.Point(40, 40),
            labelClass: "map-labels map-labels-"+ [obj[key][5]] +"", // the CSS class for the label
            labelInBackground: false,
            icon: ' '
        });

        var iw = new google.maps.InfoWindow({
            content: '<a href="'+ [obj[key][11]] +'"><div class="box-info-map">'+
                        '<div class="box-info-img bg-option" style=" background-image:url(\'./img/slid-s-1.jpg\') "></div>'+
                        '<div class="box-info-t">'+[obj[key][6]]+'</div>'+
                        '<div class="box-info-params">Paйон:  '+ [obj[key][7]] +'</div>'+
                        '<div class="box-info-tr">'+
                            '<div class="box-info-params box-info-td">Кол-во комнат:  '+ [obj[key][8]] +'</div>'+
                            '<div class="box-info-params __bold box-info-td __right">'+ obj[key][4] +'</div>'+
                        '</div>'+
                        '<div class="box-info-tr">'+
                            '<div class="box-info-params box-info-td">Габариты:  '+ [obj[key][8]] +'</div>'+
                            '<div class="box-info-params __bold box-info-td __right">'+ [obj[key][10]] +' кв/м</div>'+
                        '</div>'+
                    '</div></a>'
        });
        google.maps.event.addListener(marker, "click", function (e) {
            iw.open(map, this);
            // var infoBox = new InfoBox({
            //     latlng: this.getPosition(),
            //     map: map,
            //     content: this.content
            // });
        });
        //MAP_MARKERS[obj[key][5]] = marker
    }
}

function pinSymbol(color) {
    return {
        path: 'M 0,0 C -2,-20 -10,-22 -10,-30 A 10,10 0 1,1 10,-30 C 10,-22 2,-20 0,0 z',
        fillColor: color,
        fillOpacity: 1,
        strokeColor: '#000',
        strokeWeight: 2,
        scale: 2
    };
}

$(document).ready(function(){
    $(".js-hover-map").hover(function(){
        var id = $(this).data('id');
        $('.map .map-labels-'+ id +'').addClass('map-hover');
    }, function(){
        var id = $(this).data('id');
        $('.map .map-labels-'+ id +'').removeClass('map-hover');
    });
});

