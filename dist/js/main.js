$(document).ready(function(){

    //show modal form
    $('body').on('click','.js-modal-form-show', function () {
        $('#modal-form').modal('show');
        $('#modal-form input').val('');
        return false;
    });

    //main slider
    $(".js-slider-main").owlCarousel({
        loop: true,
        dots: true,
        nav: true,
        items: 1,
        navigation: false,
        navText: ['<i class="icn-arrow-left icn"></i>','<i class="icn-arrow-right icn"></i>'],
        slideBy: 2
    });

    //slider small
    $(".js-slider-small").owlCarousel({
        loop: true,
        dots: false,
        nav: true,
        // items: 4,
        margin:10,
        center: true,
        navigation: false,
        navText: ['<i class="icn-arrow-left icn"></i>','<i class="icn-arrow-right icn"></i>'],
        slideBy: 2,
        responsive: {
            0: {
                items: 1,
                center: false,
            },
            650: {
                items: 2,
                center: false,
            },
            900: {
                items: 3,
                center: false,

            },
            1200: {
                items: 4,
            }
        }
    });


    //slick small slider inner
    $('.js-slider-small-inner').slick({
        slidesToShow: 1,
        draggable: false,
        centerPadding: '60px',
        dots: false,
        infinite: true,
        speed: 500,
        fade: true,
        nextArrow: '<div class="slick-next-custom"><i class="icn-arrow-s-right-small icn"></i></div>',
        prevArrow: '<div class=" slick-prev-custom"><i class="icn-arrow-s-left-small icn"></i></div>'
    });

    //single_apartment
    $('#apartment-slider-big').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        asNavFor: '#apartment-slider-small',
        dots: false,
        focusOnSelect: true,
        nextArrow: '<div class="apart-slick-next-custom"><i class="icn-arrow-s-right-small icn"></i></div>',
        prevArrow: '<div class="apart-slick-prev-custom"><i class="icn-arrow-s-left-small icn"></i></i></div>'
    });

    $('#apartment-slider-small').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        vertical: true,
        arrows: false,
        centerPadding: '10px',
        // fade: true,
        asNavFor: '#apartment-slider-big'
    });

    // $('#vertical').lightSlider({
    //     gallery:true,
    //     item:1,
    //     vertical:true,
    //     verticalHeight:295,
    //     adaptiveHeight:true,
    //     vThumbWidth:170,
    //     thumbItem:3,
    //     thumbMargin:10,
    //     slideMargin:0,
    //     loop: true
    // });

    // scroll main page (first block)
    $('.scroll-wrap').click(function () {
        $('html').animate({
                scrollTop: $('.poin-scroll').offset().top
            },
            800);
        return false;
    });

    //select 2
    $('.js-select').select2({
        minimumResultsForSearch: Infinity,
        placeholder: 'asd'
    });

    //select mob ver
    $('.js-select-mob').selectric();

    //show search on main page
    $('body').on('click','.js-search-icon',function () {
        $('.mp-slider-main-box').toggleClass('open');
        $('.js-search-icon i').toggleClass('hidden');
    });

    //show mob search on main page
    $('.js-search-mob-icon').on('click',function () {
        $('#modal-search').modal('show');
    });

    $('.js-mob-search-more-link').on('click',function (e) {
        e.preventDefault();
        $('.js-mob-search-more-link').addClass('active');
        $(this).removeClass('active');
        $('.advanced-mob-search-box').slideToggle();
    });

    //show filtre price
    $('body').on('click','.js-cat-price-select', function () {
        // $(this).toggleClass('open')
        $('.cat-custom-select-box').toggleClass('open')
    });

    $(document).click(function(e) {

        if($('.js-cat-price-select').is(e.target)){
            return false
        }

        if (! $('.cat-custom-select-box').is(e.target) &&  $('.cat-custom-select-box').has(e.target).length === 0) {
            if($('.cat-custom-select-box').is(":visible")) {
                $('.cat-custom-select-box').removeClass('open')
            }
        }
    });

    //custom range
    jQuery(function() {

        if(!$('#custom-range').length > 0) return false

        var keypressSlider = document.getElementById('custom-range');
        var inputs = [document.getElementById('custom-range-from'), document.getElementById('custom-range-to')];
        var min = $('#custom-range').data('range-to');
        var max = $('#custom-range').data('range-from');
        customRange(keypressSlider, inputs, false, min, max);
    });

    //custom range2
    jQuery(function() {

        if(!$('.js-range-areas').length > 0) return false

        var keypressSlider = document.getElementsByClassName('js-range-areas')[0];
        var inputs = [document.getElementsByClassName('js-range-areas-from')[0], document.getElementsByClassName('js-range-areas-to')[0]];
        var min = $('.js-range-areas').data('range-to');
        var max = $('.js-range-areas').data('range-from');
        customRange(keypressSlider, inputs, true, min, max);
    });

    // scroll map
    jQuery(function() {
        if($('.js-box-map').length > 0 ){
            var  heightTop;
            var footerTop;

            $( window ).scroll(function() {

                if(!heightTop ){
                    heightTop = $('.js-box-map').offset().top
                }
                if(!footerTop ){
                    footerTop = $('footer').offset().top - 113
                }

                //fixed map
                if ( heightTop <= $(window).scrollTop() &&  !$('.js-box-map').hasClass('fixed-map')) {

                    var width_box =  $('#js-map-container').width();
                    $('.js-box-map').addClass('fixed-map');
                    $('#js-map-container').width(width_box);

                } else if ( heightTop > $(window).scrollTop() &&  $('.js-box-map').hasClass('fixed-map')) {

                    $('.js-box-map').removeClass('fixed-map');

                }

                //footer
                if (footerTop - $(window).height() <= $(window).scrollTop()){
                        $('.box-map').css('bottom', $(window).scrollTop() - (footerTop - $(window).height()))
                } else if ( parseFloat($('.box-map').css('bottom')) > 0) {
                    $('.box-map').css('bottom', 0);
                }

            })
        }
    })

    //show advanced search
    $('body').on('click','.js-mp-advanced-search',function (e) {
        e.preventDefault();
        $('.js-mp-advanced-search').addClass('active');
        $(this).removeClass('active')
        // if($('.advanced-search-box').is(":visible")){
        //     $(this).text('РАСШИРЕННЫЙ ПОИСК');
        // } else  {
        //     $(this).text('ОБЫЧНЫЙ ПОИСК');
        // }
        $('.advanced-search-box').slideToggle();
    });


    //open menu
    $('body').on('click','.js-header-menu-open', function () {
        $('.header-menu-box').slideToggle();
        $('.header-menu-icon-box').toggleClass('open');
    });

    //close menu
    $(document).on('click', function (e) {
        if($(e.target).closest('.js-header-menu-open').length) {
            return false
        }
        if (!$(e.target).closest(".header-menu-box").length) {
            $('.header-menu-box').slideUp();
            $('.header-menu-icon-box').removeClass('open');
        }


        if (!$(e.target).closest(".info-modal-box").length) {
            $('.info-modal-box').removeClass('open')
        }


        if (!$(e.target).closest(".share-with-box").length) {
            $('.share-with-box').removeClass('open')
        }

    });

    // close menu
    $('body').on('click','.js-close-menu', function () {
        $('.header-menu-box').slideUp();
        $('.header-menu-icon-box').removeClass('open');
    });

    //video play
    $('body').on('click','.js-spec-play', function () {
        $('.spec-video').get(0).play();
        $('.spec-video').addClass('open');

    });

    //video pause
    $('body').on('click','.spec-video', function () {
        $('.spec-video').get(0).pause();
        $('.spec-video').removeClass('open');
    });

    // show info block
    $('body').on('click','.js-info-modal-icon', function (e) {
        e.preventDefault();
       $(this).closest('.info-modal-box').toggleClass('open');
    });

    $('body').on('click','.js-share-with', function (e) {
        e.preventDefault();
        $(this).closest('.share-with-box').toggleClass('open');
    });

    //vacancy text more
    $('body').on('click','.js-vacancy-single-content-more', function () {
        var thisParent = $(this).closest('.vacancy-single');
        thisParent.toggleClass('open');
        thisParent.find('.vacancy-single-content-half').slideToggle();
        if(thisParent.hasClass('open')){
            thisParent.find('.js-vacancy-single-content-more-text').text('Свернуть');
        } else {
            thisParent.find('.js-vacancy-single-content-more-text').text('Подобнее');
        }
    });

    /*__daterangepicker__*/
    if(typeof daterangepicker !== 'undefined') {
        $('input.daterange').daterangepicker({
            //        autoUpdateInput: false,
            singleDatePicker: true,
            locale: {
                format: 'MM.DD.YYYY',
                applyLabel: "Применить",
                cancelLabel: "Отмена",
                fromLabel: "From",
                toLabel: "To",
                customRangeLabel: "Custom",
                daysOfWeek: [
                    "Вс",
                    "Пн",
                    "Вт",
                    "Ср",
                    "Чт",
                    "Пт",
                    "Сб"
                ],
                monthNames: [
                    "Январь",
                    "Февраль",
                    "Март",
                    "Апрель",
                    "Май",
                    "Июнь",
                    "Июль",
                    "Август",
                    "Cентябрь",
                    "Октября",
                    "Ноябрь",
                    "Декабрь"
                ],
                firstDay: 1
            }
        });
    }

});

function customRange(keypressSlider, inputs, tooltips, min, max ) {

    noUiSlider.create(keypressSlider, {
        start: [min, max],
        connect: true,
        // direction: 'rtl',
        tooltips: false,
        step: 5,
        tooltips: tooltips,
        //tooltips: [tooltips, wNumb({ decimals: 1}) ],
        range: {
            'min': min,
            // '10%': [10, 10],
            // '50%': [80, 50],
            // '80%': 150,
            'max': max
        }
    });

    keypressSlider.noUiSlider.on('update', function (values, handle) {
        inputs[handle].value = values[handle];
    });
    function setSliderHandle(i, value) {
        var r = [null, null];
        r[i] = value;
        keypressSlider.noUiSlider.set(r);
    }

    // Listen to keydown events on the input field.
    inputs.forEach(function (input, handle) {

        input.addEventListener('change', function () {
            setSliderHandle(handle, this.value);
        });

        input.addEventListener('keydown', function (e) {

            var values = keypressSlider.noUiSlider.get();
            var value = Number(values[handle]);

            // [[handle0_down, handle0_up], [handle1_down, handle1_up]]
            var steps = keypressSlider.noUiSlider.steps();

            // [down, up]
            var step = steps[handle];

            var position;

            // 13 is enter,
            // 38 is key up,
            // 40 is key down.
            switch (e.which) {

                case 13:
                    setSliderHandle(handle, this.value);
                    break;

                case 38:

                    // Get step to go increase slider value (up)
                    position = step[1];

                    // false = no step is set
                    if (position === false) {
                        position = 1;
                    }

                    // null = edge of slider
                    if (position !== null) {
                        setSliderHandle(handle, value + position);
                    }

                    break;

                case 40:

                    position = step[0];

                    if (position === false) {
                        position = 1;
                    }

                    if (position !== null) {
                        setSliderHandle(handle, value - position);
                    }

                    break;
            }
        });
    });

}

//preloader
$(window).on('load',function() {
    $('#preloader').fadeOut();
});

